package com.example.demo;

import com.example.demo.Base.LuasBangun;
import com.example.demo.Base.VolumeBangun;
import com.example.demo.model.Mobil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);


        }

}
