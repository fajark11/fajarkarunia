package com.example.demo.controller;

import com.example.demo.Base.LuasBangun;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/bangunruang")
//nama request itu url
public class BangunRuangController {

    @RequestMapping(value = {"/",""},method = RequestMethod.GET)
    public String index (){
//        namafolder/requestmapping
        return "bangunruang/index";

    }
    @RequestMapping(value={"/segitiga"},method = RequestMethod.GET )
    public String segitiga(){
        return "bangunruang/segitiga";

    }
    @RequestMapping(value={"/luas/{abc}"},method = RequestMethod.GET )
//    didalam kurung parameter
    public String luas(Model mdl, @PathVariable("abc") String fajar){
//petik pertama untuk subject, petik kedua itu isi pesan
// object bisa di isi bigdecimal/string
        mdl.addAttribute("luas",new LuasBangun());
        mdl.addAttribute("title",fajar);
        return "bangunruang/luas";

    }
    @RequestMapping(value={"/luas"},method = RequestMethod.POST )
//    didalam kurung parameter
//    samping nama class itu suggest
    public String luasHasil(@ModelAttribute LuasBangun luasBangun){//model untuk lempar data,
        return "bangunruang/luas-hasil";

    }

}
