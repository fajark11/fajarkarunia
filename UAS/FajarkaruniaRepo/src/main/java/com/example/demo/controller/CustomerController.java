package com.example.demo.controller;

import com.example.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @RequestMapping(value = {"","/"})
    public String List(Model model) {
        model.addAttribute("cs", customerService.findAll());
        return "customer/list";
    }

    @RequestMapping(value= {"/create"}, method = RequestMethod.GET)
    public String save(Model model) {
        model.addAttribute("cs", new com.example.demo.model.Customer());
        return "customer/insert";
    }

    @RequestMapping(value= {"/create"}, method = RequestMethod.POST)
    public String save(Model model, com.example.demo.model.Customer customer) {
        model.addAttribute("cs", customerService.Save(customer));
        return "redirect:/customer";
    }

    @RequestMapping(value= {"/edit/{id}"}, method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model) {
        model.addAttribute("cs", customerService.getId(id));
        return "customer/edit";
    }

    @RequestMapping(value= {"/delete/{id}"})
    public String delete(@PathVariable Integer id) {
        customerService.del(id);
        return "redirect:/customer";
    }
}
