package com.example.demo.services;

import com.example.demo.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Service
@Transactional
public class CustomerService {

    private EntityManagerFactory jar;

    @Autowired
    public void setJar(EntityManagerFactory jar) {
        this.jar = jar;
    }



    @Autowired
    CustomerRepository customerRepository;

    public List<com.example.demo.model.Customer> findAll() {return customerRepository.findAll();}

    public com.example.demo.model.Customer Save(com.example.demo.model.Customer customer) {
        EntityManager em = jar.createEntityManager();
        em.getTransaction().begin();
        com.example.demo.model.Customer simpan = em.merge(customer);
        em.getTransaction().commit();
        return simpan;
    }

    public com.example.demo.model.Customer getId(Integer id) {
        EntityManager em = jar.createEntityManager();
        return  em.find(com.example.demo.model.Customer.class, id);
    }

    public void del(Integer id) {
        EntityManager em = jar.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(com.example.demo.model.Customer.class, id));
        em.getTransaction().commit();
    }
}
