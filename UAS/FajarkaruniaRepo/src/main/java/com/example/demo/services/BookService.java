package com.example.demo.services;

import com.example.demo.model.Book;
import com.example.demo.repositories.BookRepository;
import org.hibernate.sql.Insert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;


@Service
@Transactional
public class BookService {
    private EntityManagerFactory jar;

    @Autowired
    public void setJar(EntityManagerFactory jar) {
        this.jar = jar;
    }

    @Autowired
    BookRepository bookRepository;
//    list
    public List<Book> findAll(){
        return bookRepository.findAll();
    }

    public Book findBookById(Integer id){
        return bookRepository.findBookById(id);
    }
    public Book bookSave(Book book){
        return bookRepository.save(book);
    }

    public void deleteBook(Integer id){
    Book book = bookRepository.findBookById(id);
        try {
        bookRepository.delete(book);
        }catch (Exception exception) {
            System.out.print(exception.getMessage());
        }
    }

    public Book simpan(Book book){
        EntityManager entityManager= jar.createEntityManager();
        entityManager.getTransaction().begin();
        Book berhasil= entityManager.merge(book);
        entityManager.getTransaction().commit();
        return  berhasil;
    }
}
