package com.example.demo.Base;

public class LuasBangun {
    private float pjg;
    private float lbr;

   public LuasBangun(float pjg,float lbr){
       this.pjg=pjg;
       this.lbr=lbr;
   }

   public LuasBangun(){

   }

    public void setPjg(float pjg) {
        this.pjg = pjg;
    }

    public void setLbr(float lbr) {
        this.lbr = lbr;
    }

    public float hitungluas(){
       float Luas=pjg*lbr;
       return Luas;
    }
}
