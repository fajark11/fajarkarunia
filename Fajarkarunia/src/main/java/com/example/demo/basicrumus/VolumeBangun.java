package com.example.demo.basicrumus;


public class VolumeBangun extends LuasBangun {
    private float tinggi;


    public VolumeBangun(float tinggi,float pjg,float lbr) {
        this.tinggi = tinggi;
        this.pjg = pjg;
        this.lbr= lbr;

    }
    public float ItungVolume(){
        float vlm=pjg*lbr*tinggi;
        return  vlm;
    }

}
