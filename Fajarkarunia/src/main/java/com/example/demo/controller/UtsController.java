package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/UTS")
public class UtsController {
    @RequestMapping(value = {"/",""},method = RequestMethod.GET)
    public String index(){
        return "UTS/index";
    }
}
