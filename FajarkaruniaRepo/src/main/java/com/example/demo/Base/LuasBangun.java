package com.example.demo.Base;

public class LuasBangun {
    private Integer panjang;
    private Integer lebar;

   public LuasBangun(Integer panjang,Integer lebar){
       this.panjang=panjang;
       this.lebar=lebar;
   }

   public LuasBangun(){
   }

    public Integer hitungluas(){
       Integer Luas=panjang*lebar;
       return Luas;
    }

    public Integer getPanjang() {
        return panjang;
    }

    public void setPanjang(Integer panjang) {

       this.panjang = panjang;
    }

    public Integer getLebar() {
        return lebar;
    }

    public void setLebar(Integer lebar) {

       this.lebar = lebar;
    }
}
