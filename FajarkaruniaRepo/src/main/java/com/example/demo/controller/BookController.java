package com.example.demo.controller;


import com.example.demo.model.Book;
import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

@RequestMapping("/book")
public class BookController {
    @Autowired
    BookService bookService;

    @RequestMapping(value = {"", "/"})
    public String listBook(Model model) {
        model.addAttribute("Books", bookService.findAll());
        return "books/list";
    }

    //    controller,service,Repostitory\
    @RequestMapping(value = {"/create"}, method = RequestMethod.GET)
    public String create(Model model) {
    model.addAttribute("Books",new Book());
        return "books/insert";
    }
//    cara 1
   @RequestMapping(value = {"/create"},method = RequestMethod.POST)
        public String save(Model model, Book book){
        model.addAttribute("Books",bookService.simpan(book));
        return "redirect:/book";
   }
//   cara2
//   @RequestMapping(value = {"/created"},method = RequestMethod.POST)
//    public String saveBook (@ModelAttribute Book book, Model model){
//        Book savedbook=bookService.bookSave(book);
//        return "redirect:/book";
//   }
   @RequestMapping(value = {"/edit/{id}"},method = RequestMethod.GET)
    public String editBook(Model model, @PathVariable("id") Integer id){
        Book book  =bookService.findBookById(id);
        model.addAttribute("Books",book);
        return "books/edit";
   }
    @RequestMapping(value = {"/delete/{id}"},method = RequestMethod.GET)
    public String deleteBook (@PathVariable("id") Integer id){
        bookService.deleteBook(id);
        return "redirect:/book";
    }

}






